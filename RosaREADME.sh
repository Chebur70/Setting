"GrubThemesRosa"

#Install GrubThemesRosaDebian
				echo "================ Установка GrubThemesRosa ================"								 
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/Rosa.tar.gz
                                tar xvf Rosa.tar.gz && rm -rf Rosa.tar.gz   
                                sudo mkdir /boot/grub/themes/     
				mv rosa/GrubThemesRosaUninstall.sh ~/'Desktop'/   или   ~/'Рабочий стол'/                                                      
                                sudo mv rosa /boot/grub/themes/rosa                                                                                                                                                                                                                                                              
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz                                    
                                sudo mv grub /etc/default/grub                                                          
                                sudo update-grub

#Install GrubThemesRosaLMDE
				echo "================ Установка GrubThemesRosa ================"								 
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/Rosa.tar.gz
                                tar xvf Rosa.tar.gz && rm -rf Rosa.tar.gz   
                                sudo mkdir /boot/grub/themes/     
				mv rosa/GrubThemesRosaUninstall.sh ~/'Desktop'/   или   ~/'Рабочий стол'/                                              
                                sudo mv rosa /boot/grub/themes/rosa                                                                                                                                                                                                                                                           
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz     
                                sudo mv grub /etc/default/grub                                                          
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/60_mint-theme.cfg.tar.gz
				tar xvf 60_mint-theme.cfg.tar.gz && rm -rf 60_mint-theme.cfg.tar.gz
				sudo mv 60_mint-theme.cfg /etc/default/grub.d/60_mint-theme.cfg                                                       
                                sudo update-grub

#Install GrubThemesRosaLinuxMint
				echo "================ Установка Grub themes Rosa ================"				
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/Rosa.tar.gz
                                tar xvf Rosa.tar.gz && rm -rf Rosa.tar.gz   
                                sudo mkdir /boot/grub/themes/     
				mv rosa/GrubThemesRosaUninstall.sh ~/'Desktop'/   или   ~/'Рабочий стол'/                                                  
                                sudo mv rosa /boot/grub/themes/rosa                                                                                                                                                                                                                   
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz     
                                sudo mv grub /etc/default/grub                                                          
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/50_linuxmint.cfg.tar.gz
				tar xvf 50_linuxmint.cfg.tar.gz && rm -rf 50_linuxmint.cfg.tar.gz
				sudo mv 50_linuxmint.cfg /etc/default/grub.d/50_linuxmint.cfg                                                      
                                sudo update-grub